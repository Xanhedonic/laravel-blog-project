@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a class="btn btn-info" href="/posts/create">Naujas Postas +</a>
                    <h3>Jūsų Blogo Postai</h3>
                    @if(count($posts) > 0)
                        <table class="table table-striped table-dark">
                            <tr>
                                <th>Title</th>
                                <th>Redaguoti</th>
                                <th>Trinti</th>
                            </tr>
                            @foreach ($posts as $post)
                                <tr>
                                    <td>{{$post->title}}</td>
                                    <td><a href="/posts/{{$post->id}}/edit" class="btn btn-warning">Redaguoti</a></td>
                                    <td>
                                        <form method="POST" class="delete_form float-right" action="{{ action('PostsController@destroy', $post->id) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger">X</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>Neturite jokių postų</p>
                    @endif
                    <hr>
                    <small>You are logged in!</small>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.delete_form').on('submit', function() {
            if(confirm("Ar tikrai norite panaikinti?")) {
                return true;
            } else {
                return false;
            }
        })
    });
</script>
@endsection

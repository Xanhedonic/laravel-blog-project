@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-secondary mb-3">Atgal</a>
    <h1 class="jumbotron text-center">{{$post->title}}</h1>
    <img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}" alt="">
    <br><br>
    <div>
        <!-- Kad Parsintu posta su visokiais bold, italic, listais ir pns...
            reikia taip, su dviem sauktukais: {//{!//!$post->body!!}} ignore "//" -->
        {!!$post->body!!}
    </div>
    <hr>
    <small>Sukurtas: {{$post->created_at}} Vartotojo: {{$post->user->name}}</small>
    <hr>
    @if (!Auth::guest())
        @if (Auth::user()->id == $post->user_id)
            <a href="/posts/{{$post->id}}/edit" class="btn btn-outline-warning">Redaguoti</a>

            <form method="POST" class="delete_form float-right" action="{{ action('PostsController@destroy', $post->id) }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger">X</button>
            </form>
        @endif
    @endif

    <script>
        $(document).ready(function() {
            $('.delete_form').on('submit', function() {
                if(confirm("Ar tikrai norite panaikinti?")) {
                    return true;
                } else {
                    return false;
                }
            })
        });
    </script>
@endsection
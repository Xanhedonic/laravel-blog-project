@extends('layouts.app')

@section('content')
    <h1>Edit Post</h1>
    <form method="post" action="{{ route('posts.update', $post->id) }}" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="form-group">
            @csrf            
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" value="{{$post->title}}"/>
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea class="form-control" id="editor1" name="body" cols="30" rows="10">{{$post->body}}</textarea>
        </div>
        <div class="form-group">
            <input type="file" name="cover_image">
        </div>

        <button type="submit" class="btn btn-warning">Redaguoti</button>
    </form>
@endsection
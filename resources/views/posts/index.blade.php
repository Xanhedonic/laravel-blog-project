@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
    @if(count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card p-3 mt-3 mb-3 bg-secondary text-white">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}" alt="">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a class="text-white" href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                        <small>Sukurtas: {{$post->created_at}} Vartotojo: {{$post->user->name}}</small>
                    </div>
                </div>
            </div>
        @endforeach
        <!-- Pagination linkai is PostsController -->
        {{$posts->links()}}
    @else
        <p>Nėra jokių postų</p>
    @endif
@endsection
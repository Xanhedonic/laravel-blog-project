<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        $title = 'Sveiki atvyke į Laravel Blogą';
        //return view('pages.index', compact('title'));
        return view('pages.index')->with('title', $title);
    }
    public function about() {
        $title = 'Apie';
        return view('pages.about')->with('title', $title);
    }
    public function services() {
        $data = array(
            'title' => 'Paslaugos',
            'services' => ['Web Design', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
//Nenaudojant Eloquent, galima naudoti pacias SQL Queries
use DB;

class PostsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //Idėti galimybe svečiams matyti postus, bet ne kurti ar trinti. su exceptions: ['except' => ['index', 'show]] 
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    //index - listing of all the posts
    //create - function to represent the create form \ add posts
    //store - function to submit to the model to the database
    //edit - to edit posts with edit form, which then submits to update method(function)
    //update - actually updates to the database
    //show - to show a single post
    //estrou - function to delete posts
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts = Post::all();

        //Galima padaryti, kad rodytu by order
        //$posts = Post::orderBy('title', 'asc')->get();
        //$posts = Post::orderBy('created_at', 'desc')->get();

        //Galima riboti, kiek rodys vienu metu
        //$posts = Post::orderBy('created_at', 'desc')->take(1)->get();

        //Ideti puslapius, kurie riboja, kiek rodys postu per puslapi, naudojant Paginate, bet reikia ideti papildomai linka i postu blade.php
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);

        //Galima, kad rodytu individualiai kazkoki nurodant specifiskai
        //return Post::where('title', 'Post One')->get();

        //Nauojant DB Queries
        //$posts = DB::select('SELECT * FROM posts');

        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Handle File Upload
        if($request->hasFile('cover_image')) {
            // Get filename with the extension
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.png';
        }

        // Create Post
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/posts')->with('success', 'Postas Sukurtas Sėkmingai!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        //Check the correct user for ability to edit
        if(auth()->user()->id !== $post->user_id) {
            return redirect('/posts')->with('error', 'Jūs nesate šio posto autorius');
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Handle File Upload
        if($request->hasFile('cover_image')) {
            // Get filename with the extension
            $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        }

        // Create Post
        $post = Post::find($id);

        //Check the correct user for ability to update, in case of csrf defence breach
        if(auth()->user()->id !== $post->user_id) {
            return redirect('/posts')->with('error', 'Jūs nesate šio posto autorius');
        }

        $post->title = $request->input('title');
        $post->body = $request->input('body');
        if($request->hasFile('cover_image')) {
            $post->cover_image = $fileNameToStore;
        }

        $post->save();

        return redirect('/posts')->with('success', 'Postas Paredaguotas!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        //Check the correct user for ability to delete
        if(auth()->user()->id !== $post->user_id) {
            return redirect('/posts')->with('error', 'Jūs nesate šio posto autorius');
        }

        // Kai postas trinamas, kad issitrintu ir pats uploadintas paveikslas
        if($post->cover_image != 'noimage.png') {
            // Delete the image
            Storage::delete('public/cover_images/'.$post->cover_image);
        }

        $post->delete();
        return redirect('/posts')->with('success', 'Postas Sunaikintas');
    }
}

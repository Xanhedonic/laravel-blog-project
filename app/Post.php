<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // Table Name, default postu pavadinimai butu "Posts" nes sukurem model pavadinimu "Post", bet galima pakeisti:
    protected $table = 'posts';
    // Taip pat galima pakeisti Primary Key field
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    //Sukurti Relationship, Postai, kurie priklauso atitinkamui accountui
    public function user() {
        return $this->belongsTo('App\User');
    }
}

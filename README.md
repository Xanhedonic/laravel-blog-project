## Laravel projektas blogas
Laravel projektas, blog su nuotraukų įkėlimų, postų redagavimu, trynimu ir vartotojų užsiregistravimu. MySQL, PHP, Laravel framework.

## Setup
1. Sukurti duomenų bazę pavadinimu "laravel_blog";
2. Terminale duoti komandą "php artisan migrate", kad susikurtų lentelės duomenų bazėje;
3. //ARBA: Vietoj 1. ir 2. punktų: Tiesiog, importuoti laravel_blog.sql failą iš _SQL/ folder į savo duomenų bazę (jame jau bus testiniai postai ir komentarai su visomis lentelėmis);
4. Terminale duoti komandą "php artisan storage:link", kad sukurtų storage link paveikslų įkėlimui. 
5. Ijungti tinklapį su komanda "php artisan serve";

### Sidenote
* Jei tinklapyje įkeltų nuotraukų nerodo, Ištrinti /storage folder in public/ folder ir vėl paleisti "php artisan storage:link" komandą;


<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 18, 2020 at 10:21 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_post_id_foreign` (`post_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `email`, `comment`, `approved`, `post_id`, `created_at`, `updated_at`) VALUES
(10, 'Jonas', 'jonas@gmail.com', 'k adjwlkja lkdja wld jaldkajkla woaw a', 1, 18, '2020-05-17 12:27:34', '2020-05-17 12:27:34'),
(11, 'Karolis', 'karolis@gmail.com', 'Karolis YEEEEEEEEEEEEEEE', 1, 18, '2020-05-17 12:31:05', '2020-05-17 12:31:05'),
(12, 'Petras', 'petras@gmail.com', 'Vienas Petras\r\nDu Petrai\r\nTrys Petrai', 1, 1, '2020-05-18 05:35:33', '2020-05-18 05:35:33'),
(13, 'Karolis', 'karolis@gmail.com', 'Karolis Karolis Karolis Karolis', 1, 1, '2020-05-18 05:36:25', '2020-05-18 05:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_04_23_170016_create_posts_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 2),
(8, '2020_05_17_100539_create_comments_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`, `user_id`, `cover_image`) VALUES
(1, 'Post One', '<p>This is the post body, <strong>Paredaguotas</strong></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis massa tincidunt dui ut. Libero enim sed faucibus turpis in. Eget nullam non nisi est sit. Magna fringilla urna porttitor rhoncus dolor. Nisl rhoncus mattis rhoncus urna neque viverra justo nec. Porttitor lacus luctus accumsan tortor posuere. Porta non pulvinar neque laoreet suspendisse interdum. At risus viverra adipiscing at in. Cras adipiscing enim eu turpis egestas pretium aenean. Eleifend quam adipiscing vitae proin sagittis. Cras fermentum odio eu feugiat pretium nibh. Platea dictumst vestibulum rhoncus est pellentesque. Adipiscing bibendum est ultricies integer quis auctor elit sed. Diam volutpat commodo sed egestas egestas fringilla phasellus faucibus scelerisque. Curabitur vitae nunc sed velit dignissim sodales. Sit amet consectetur adipiscing elit ut aliquam. Nec sagittis aliquam malesuada bibendum. Eu scelerisque felis imperdiet proin fermentum leo. Interdum velit euismod in pellentesque massa placerat duis ultricies lacus.</p>\r\n\r\n<p>Cras pulvinar mattis nunc sed blandit libero. Massa sapien faucibus et molestie ac feugiat sed. Vitae aliquet nec ullamcorper sit. Ultricies tristique nulla aliquet enim tortor at auctor. Aliquam vestibulum morbi blandit cursus risus. Purus in mollis nunc sed id semper risus in hendrerit. Augue eget arcu dictum varius duis at consectetur. Sodales ut eu sem integer vitae. In cursus turpis massa tincidunt dui. Morbi tempus iaculis urna id. Dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in.</p>\r\n\r\n<p>Diam in arcu cursus euismod quis viverra nibh cras pulvinar. Vitae tortor condimentum lacinia quis. Egestas diam in arcu cursus. Quisque non tellus orci ac. Feugiat in fermentum posuere urna. Pretium quam vulputate dignissim suspendisse in est ante in nibh. Maecenas sed enim ut sem. Sed libero enim sed faucibus turpis in eu. Diam quam nulla porttitor massa id neque aliquam vestibulum morbi. Feugiat scelerisque varius morbi enim. Augue ut lectus arcu bibendum. Sit amet consectetur adipiscing elit. Sit amet est placerat in egestas erat imperdiet sed euismod. Odio facilisis mauris sit amet. Et sollicitudin ac orci phasellus egestas tellus. Gravida quis blandit turpis cursus in. Duis at consectetur lorem donec massa sapien faucibus et. Turpis egestas sed tempus urna et pharetra pharetra. Laoreet suspendisse interdum consectetur libero.</p>\r\n\r\n<p>Phasellus egestas tellus rutrum tellus. Augue interdum velit euismod in pellentesque massa placerat. Adipiscing tristique risus nec feugiat in fermentum posuere. Nisl nisi scelerisque eu ultrices vitae auctor. Risus nec feugiat in fermentum posuere urna nec tincidunt praesent. Libero enim sed faucibus turpis in. Massa placerat duis ultricies lacus sed turpis tincidunt. Dictum varius duis at consectetur lorem donec massa sapien. Cursus turpis massa tincidunt dui ut. Ornare arcu dui vivamus arcu felis bibendum ut tristique. Cras pulvinar mattis nunc sed blandit libero. Tellus in hac habitasse platea dictumst. Neque vitae tempus quam pellentesque nec nam aliquam sem et. Interdum varius sit amet mattis vulputate enim nulla.</p>\r\n\r\n<p>Cursus turpis massa tincidunt dui ut ornare lectus sit. Suscipit adipiscing bibendum est ultricies integer. Magna fermentum iaculis eu non diam. Feugiat nibh sed pulvinar proin gravida hendrerit. Nunc sed augue lacus viverra. Dui faucibus in ornare quam viverra orci sagittis eu. Adipiscing enim eu turpis egestas pretium aenean pharetra. Ultrices dui sapien eget mi. Tortor at risus viverra adipiscing at in. Elementum integer enim neque volutpat ac. Et sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Fringilla phasellus faucibus scelerisque eleifend donec. Massa id neque aliquam vestibulum morbi blandit cursus. Faucibus pulvinar elementum integer enim neque. Elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Volutpat ac tincidunt vitae semper quis lectus nulla. Nibh mauris cursus mattis molestie a iaculis at. Velit laoreet id donec ultrices tincidunt arcu non sodales. Non curabitur gravida arcu ac.</p>', '2020-04-23 14:20:30', '2020-05-18 05:01:07', 1, 'Outer-Space-Planets_1589788705.jpg'),
(2, 'Antras Postas', 'Cia yra antras postas padarytas su Eloquent komandom', '2020-04-23 14:23:22', '2020-04-23 14:23:22', 1, ''),
(3, 'Trečias postas :)', 'Trečias posts, sukurtas per patį tinklapį, PostsController store metodu. eyeyyyy', '2020-04-25 12:23:16', '2020-04-25 12:23:16', 1, ''),
(4, 'Postas Keturi. 4', 'Super Sonic Speeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeed, GOTTA GO FAST', '2020-04-25 12:30:21', '2020-04-25 12:30:21', 1, ''),
(5, 'Penktas turbut, CKEditor testing', '<p>Testing CKEditor, su <strong>bold</strong>&nbsp;,&nbsp;<em>Italic, <s>I&scaron;brauktas</s></em>&nbsp;<br />\r\nListas:</p>\r\n\r\n<ol>\r\n	<li>Vienas;</li>\r\n	<li>2+2=4.</li>\r\n</ol>\r\n\r\n<p>Ta&scaron;kinis listas:</p>\r\n\r\n<ul>\r\n	<li>Vilnius;</li>\r\n	<li>Mažeikiai;</li>\r\n	<li>Klaipėda.</li>\r\n</ul>', '2020-04-25 14:06:36', '2020-05-10 06:57:18', 1, ''),
(6, 'Šeštas postas, dėl Pagination trigger', '<h1>Heading 1&nbsp;</h1>\r\n\r\n<h2>Heading 2&nbsp;</h2>\r\n\r\n<blockquote>\r\n<p>Block Quote</p>\r\n</blockquote>\r\n\r\n<p><kbd>Kitoks Stilius&nbsp;</kbd></p>\r\n\r\n<p>&nbsp;</p>', '2020-04-26 12:43:07', '2020-05-10 06:56:21', 1, ''),
(15, 'Fast Cat gif', '<p><img alt=\"\" src=\"https://gifimage.net/wp-content/uploads/2017/10/cat-typing-fast-gif-2.gif\" style=\"height:212px; width:220px\" /></p>\r\n\r\n<p><strong>Fast Cat</strong></p>', '2020-04-29 13:50:56', '2020-04-29 13:58:46', 1, ''),
(10, '7-tas Postas', '<p>Septintas, įdėjus <strong>user_id&nbsp;&nbsp;</strong>į DB lentelę <strong>posts</strong></p>', '2020-04-27 14:48:53', '2020-04-27 14:48:53', 1, ''),
(11, 'Antro accounto postas', '<p>Testing second <strong>account. <em>Antras</em></strong></p>', '2020-04-28 06:08:32', '2020-04-30 05:18:54', 2, ''),
(18, 'Image Upload Test', '<p>Paveiklsas&nbsp;<br />\r\n<strong>YEET</strong></p>', '2020-04-29 15:14:22', '2020-04-30 05:17:53', 2, 'NeonCityStreet_1588184062.jpg'),
(20, 'Antras Paveikslas test', '<p><strong>Rip and Tear ALL the Demons</strong></p>', '2020-04-29 15:46:09', '2020-04-29 15:46:09', 2, 'NeonCityNight_1588185969.jpg'),
(23, 'no image test', '<p>Postas be paveikslo testas, bet paveikslas yra: <strong>&quot;noimage.png&quot;</strong></p>', '2020-04-29 16:33:08', '2020-04-30 05:19:17', 2, 'noimage.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Robert', 'testas@gmail.com', NULL, '$2y$10$rmTZC2vZt5479w5wrgqFhuszZkTypRLrtjOC1kD6KhffEmF.RlmHu', NULL, '2020-04-27 13:45:41', '2020-04-27 13:45:41'),
(2, 'Antras', 'antras@gmail.com', NULL, '$2y$10$Ojno6DVR4Kjwdawc5fBkx.AyUHOBmswcSmskVQPs4dcl2mMEUsjsu', NULL, '2020-04-28 05:59:52', '2020-04-28 05:59:52');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
